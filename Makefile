mojo-run:
	DBIC_TRACE=1 carton exec -- morbo ./balik.pl -l http://*:3456

mojo-prod-run:
	DBIC_TRACE=1 MOJO_MODE=production carton exec -- morbo ./balik.pl -l http://*:3456

prod-run:
	carton exec -- hypnotoad balik.pl

test:
	carton exec -- prove -lv
