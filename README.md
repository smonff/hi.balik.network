# Description

This is a personal branding [website homepage](https://hi.balik.network). It's aim is too keep it simple.

I use it as a convenient way to give informations about different projects. It use a minimal dependencies stack, thanks to some very powerful tools.
With mostly [Mojolicious](https://mojolicious.org), [jQuery](https://jquery.com/) and [KNACSS](https://www.knacss.com/), upgrading a couple of informations on a website remains a pleasure and not a dependencies hell.

The backend is made with Mojolicious. Simple to install and deploy, full toolkit including a powerful template engine. It's extended with [an I18N plugin](https://metacpan.org/pod/distribution/Mojolicious-Plugin-I18N/README.pod) that makes very easy to deal with the different languages available.

Historically I used this projet to experiment and train myself on the *[MEAN](https://en.wikipedia.org/wiki/MEAN_(software_bundle)) JavaScript ecosytem* but this project don''t reflect this anymore. If you look at the pre-2019 git history, you can see how it was before (use of [Node.js](http://nodejs.org/), [Express](https://expressjs.comExpress.js), Bootstrap, etc.). I consider this stack being too bloated for such a simple website and Perl seems to be much more reliable on the long term.

## Installation

You can fork it on [Codeberg](https://codeberg.org/smonff/balik.shurf.pl).

A minimum installation on some Linux box would require:

    $ cd hi.balik.network
    $ apt install carton
    $ carton install

All CSS and JavaScript dependencies are pre-packaged, no need to fetch them from NPM.

Super-sophisticated deployment strategy on the server:

    $ git checkout master && git pull origin master
    $ make prod-run

The server should start localy thanks to [hypnotoad](https://docs.mojolicious.org/Mojo/Server/Hypnotoad). Please use a reverse proxy on your favorite server frontend (I use Apache, but feel free to use whatever you want).

# Acknowledgments

Seriously, I would like to thank:

- Ezgi, who allowed me to really achieve an extreme self-marketing
- Mathilde who reported an awful bug that turned out to be a missing comma ;D
- *« Somebody »* reported an issue with i18n, can't remember who, but I think of you <3!

Thank you!

# License

Copyright (C) 2015-2021 Sébastien Feugère

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

The content is also licensed under Creative Commons.

# Contact

* sebastien [ at ] feugere.net
