[![pipeline status](https://gitlab.com/smonff/hi.balik.network/badges/master/pipeline.svg)](https://gitlab.com/smonff/hi.balik.network/commits/master)

# Description
This is a personal branding [website homepage](https://hi.balik.network). It's aim is too keep it simple.

I use it as a convenient way to give informations about different projects. It use a minimal dependencies stack, especially on the frontend
with mostly [jQuery](https://jquery.com/) and [KNACSS](https://www.knacss.com/): I choosed to limit myself to those since this project is so simple, I didn't want to download 30 000 dependencies by installing React and have to deal with dependency hell when upgrading my CV every year.

The backend is made with Mojolicious. Simple to install and deploy, full toolkit including a powerful template engine. I extended it with an I18N plugin that makes very easy to deal with the different languages.

Historically I used this projet to experiment and train myself on the *[MEAN](https://en.wikipedia.org/wiki/MEAN_(software_bundle)) JavaScript ecosytem* but this project don''t reflect this anymore. If you look at the pre-2019 git history, you can see how it was before ([Node.js](http://nodejs.org/), [Express](https://expressjs.comExpress.js), Bootstrap, etc.)

## Installation
You can fork it
on [Gitlab](https://gitlab.com/smonff/balik.shurf.pl).

A minimum installation on Some Linux would require:

    $ cd hi.balik.network
    $ apt install libmojolicious-perl carton
    $ carton install

All CSS and JavaScript dependencies are pre-packaged, no need to fetch them from NPM. Will maybe change in the future.

Deployment strategy with SystemD:

    $ git checkout master && git pull origin master
    // Set $balik_home in makefile before
    $ cd ${balik_home}
    $ make dependencies-install
    $ systemctl start express

For more information on how to set up a process manager and SystemD,
see
[Express documentation](https://expressjs.com/en/advanced/pm.html#systemd) and
[this Axllent blog post](https://www.axllent.org/docs/view/nodejs-service-with-systemd/).

# License
Copyright (C) 2017-2020 Sébastien Feugère

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

The content is licensed under Creative Commons.

# Contact
* sebastien [ at ] feugere.net
