#!/usr/bin/env perl
use v5.20;
use feature qw/say switch/;
use Mojolicious::Lite -signatures;
# If you don't, wont load modules from lib/
use lib './lib';
use Time::Piece;
use Data::Printer;
use Encode;
use Time::HiRes qw/ clock gettimeofday/;
use HTTP::BrowserDetect;
no if ($] >= 5.018), 'warnings' => 'experimental';

our $VERSION = '2.10';

plugin 'Config';

plugin 'SecurityHeader' => [
  'Access-Control-Allow-Origin' => '*',
  'Access-Control-Allow-Headers' => 'Content-Type',
  'Access-Control-Allow-Methods' => 'GET, POST, OPTIONS, DELETE',
];

plugin I18N => {
  default => 'en',
  namespace => 'Balik::I18N',
  support_url_langs => [qw(en fr)]
};

plugin AssetPack => {
  pipes => [qw(JavaScript Css Png Fetch)]
};

app->asset->store->paths(["./public"]);

app->asset->process(
  "bundle.css" => (
    "knacss.min.css",
    "jq/jquery-ui.min.css",
    "styles.css",
  )
);

app->asset->process(
  "bundle.js" => (
    "zdog.min.js",
    "zdog.js",
    "burger.js"
  )
);

app->asset->pipe("Png")->app("pngquant");

plugin 'AutoReload' if app->mode eq 'development';

app->secrets(['9fb34e32-d50e-4cd7-81f8-4ef7c73ebdfa']);

helper get_lang => sub ($c) {
  my $browser_lang =  defined $c->req->headers->accept_language ?
    substr $c->req->headers->accept_language, 0, 2 :
    'en';
  return $browser_lang;
  # return $browser_lang !~ /en|fr/ ? 'en' : $browser_lang;
};

helper is_mobile => sub ($c) {
  my $ua = HTTP::BrowserDetect->new( $c->req->headers->user_agent );
  return $ua->mobile;
};

helper device => sub ($c) {
  return HTTP::BrowserDetect
    ->new( $c->req->headers->user_agent )
    ->device_string;
};

helper os => sub ($c) {
    return HTTP::BrowserDetect
            ->new( $c->req->headers->user_agent )
            ->os_string;
};

helper version => sub { return $VERSION };

under '/' => sub ($c) {
  $c->session->{counter}++;
  $c->timing->begin('fast');
  my $found = '';
  given ($c->url_for->to_string) {
    when (/^\/$/) { $found = 'home' }
    when (/projects$/) { $found = 'projects' }
    when ( /cv$/) { $found = 'cv' }
    when ( /contact$/ ) { $found = 'contact' }
  }
  $c->stash(
    active => $found,
    now  => localtime->year,
    time => $c->timing,
    lang => $c->get_lang
   );
};

get '/' => sub ($c) {
  $c->render(template => 'index');
};

get '/projects' => sub ($c) {
  $c->render(template => 'projects');
};

get '/cv' => sub ($c) {
  $c->render(template => 'cv');
};

get '/contact' => sub ($c) {
    $c->render(template => 'contact');
};

app->start;
