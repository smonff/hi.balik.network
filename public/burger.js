/**!
  Navigation Button Toggle class
  From https://www.knacss.com/doc.html#burger
*/
(function() {
  // old browser or not ?
  if ( !('querySelector' in document && 'addEventListener' in window) ) {
    return;
  }
  window.document.documentElement.className += ' js-enabled';
  function toggleNav() {
    // Define targets by their class or id
    var button = document.querySelector('.nav-button');
    var target = document.querySelector('body > header > nav');
    var menu   = document.querySelector('nav > ul');

    // click-touch event
    if ( button ) {
      button.addEventListener(
        'click',
        function (e) {
          button.classList.toggle('is-active');
          target.classList.toggle('is-opened');
          menu.classList.toggle('is-visible');
          e.preventDefault();
        }, false );
    }
  } // end toggleNav()
  toggleNav();
}());
