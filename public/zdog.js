// Made with Zdog

let isSpinning = true;

let illo = new Zdog.Illustration({
    element: '.zdog-canvas',
    rotate: { x: -Zdog.TAU/16 },
    dragRotate: true,
    onDragStart: function() {
        isSpinning = false;
    },
});

const distance = 40;

var dot = new Zdog.Shape({
    addTo: illo,
    translate: { y: -distance },
    stroke: 80,
    color: '#636',
});
dot.copy({
    translate: { x: -distance },
});
dot.copy({
    translate: { z: distance },
});
dot.copy({
    translate: { x: distance },
});
dot.copy({
    translate: { z: -distance },
});
dot.copy({
    translate: { y: distance },
});

function animate() {
    illo.rotate.y += isSpinning ? 0.015 : 0;
    illo.updateRenderGraph();
    requestAnimationFrame( animate );
}

animate();
