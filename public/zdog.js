// Made with Zdog

let isSpinning = true;

let illo = new Zdog.Illustration({
    element: '.zdog-canvas',
    rotate: { x: -Zdog.TAU/16 },
    dragRotate: true,
    onDragStart: function() {
        isSpinning = false;
    },
});

const distance = 10;

var dot = new Zdog.Shape({
    addTo: illo,
    translate: { y: -distance },
    stroke: 20,
    color: '#fdb8c0'/* '#fa1090' '#636'*/,
});
dot.copy({
    translate: { x: -distance },
});
dot.copy({
    translate: { z: distance },
});
dot.copy({
    translate: { x: distance },
});
dot.copy({
    translate: { z: -distance },
});
dot.copy({
    translate: { y: distance },
});

function animate() {
    illo.rotate.y += isSpinning ? 0.015 : 0;
    illo.updateRenderGraph();
    requestAnimationFrame( animate );
}

animate();
