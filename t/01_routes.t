use Mojo::Base -strict;

use Test::More tests => 12;
use Test::Mojo;
use Mojo::File 'path';

my $script = path(__FILE__)->dirname->sibling( 'balik.pl' );
my $t = Test::Mojo->new( $script );

$t->get_ok('/')->status_is(200);
$t->get_ok('/projects')->status_is(200);
$t->get_ok('/cv')->status_is(200);
$t->get_ok('/contact')->status_is(200);
$t->get_ok('/fr')->status_is(200);
$t->get_ok('/en')->status_is(200);
